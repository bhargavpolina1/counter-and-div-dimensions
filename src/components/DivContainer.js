import React, { Component } from "react";

class Divcontainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      BaseHeight: 100,
      BaseWidth: 100,
      valueToBeChanged: 10,
      radius: 0,
      counter:0
    };

    this.increaseDimensions = this.increaseDimensions.bind(this);
    this.decreaseDimensions = this.decreaseDimensions.bind(this);
    this.SwitchToCircleOrSquare = this.SwitchToCircleOrSquare.bind(this);
    this.increaseCounter = this.increaseCounter.bind(this)
    this.decreaseCounter = this.decreaseCounter.bind(this)
    this.multiplyCounter = this.multiplyCounter.bind(this)
    this.divideCounter = this.divideCounter.bind(this)
    this.resetCounter = this.resetCounter.bind(this)
  }

  increaseDimensions() {
    this.setState(prevState => ({
      BaseHeight:
        parseInt(prevState.BaseHeight) +
        parseInt(prevState.valueToBeChanged) +
        "px",
    BaseWidth:
        parseInt(prevState.BaseWidth) +
        parseInt(prevState.valueToBeChanged) +
        "px",

    }));
  }

  SwitchToCircleOrSquare(dimen) {
    this.setState({
      radius: dimen,
    });
  }
  decreaseDimensions() {
    this.setState(prevState => ({
      BaseHeight:
        parseInt(prevState.BaseHeight) -
        parseInt(prevState.valueToBeChanged) +
        "px",
      BaseWidth:
        parseInt(prevState.BaseHeight) -
        parseInt(prevState.valueToBeChanged) +
        "px",
    }));
  }

  increaseCounter(){
    console.log("Counter Increased")

    this.setState(prevState => ({
    counter:(prevState.counter)+5,
    

    }))
  }
decreaseCounter(){
  this.setState(prevState => ({
    counter: prevState.counter-1
  }))
}

multiplyCounter(){
  this.setState(prevState => ({
    counter:(prevState.counter*2)
  }))
}
resetCounter(){
  this.setState({
    counter:0
  })
}

divideCounter(){
  this.setState(prevState => ({
    counter: (prevState.counter)/2

  }))
}
  render() {
    return (
      <div>
        <div
          style={{
            height: this.state.BaseHeight,
            width: this.state.BaseWidth,
            borderColor: "black",
            borderStyle: "solid",
            backgroundColor: "green",
            borderRadius: this.state.radius,
          }}
        >{this.state.counter}</div>
        <div>
          <button onClick={this.increaseCounter}>Increase Counter</button>
          <button onClick={this.decreaseCounter}>Decrease Counter</button>
          <button onClick = {this.multiplyCounter}>Double the Counter</button>
          <button onClick = {this.divideCounter}>Half the Counter</button>
          <button onClick={this.resetCounter}>Reset Counter</button>
          <button onClick={this.increaseDimensions}>Increase Dimensions</button>
          <button onClick={this.decreaseDimensions}>Decrease Dimensions</button>
          <button
            onClick={() => {
              this.SwitchToCircleOrSquare("50%");
            }}
          >
            Switch to Circle
          </button>
          <button
            onClick={() => {
              this.SwitchToCircleOrSquare("0%");
            }}
          >
            Switch to square
          </button>
        </div>
      </div>
    );
  }
}

export default Divcontainer;
