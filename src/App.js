import logo from './logo.svg';
import './App.css';
import Divcontainer from './components/DivContainer';

function App() {
  return (
    <div className="App">
      <Divcontainer/>
    </div>
  );
}

export default App;
